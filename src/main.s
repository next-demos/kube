;;----------------------------------------------------------------------------------------------------------------------
;; Kube
;; Copyright (C)2020 Matt Davies, all right reserved.
;;----------------------------------------------------------------------------------------------------------------------

                DEVICE          ZXSPECTRUMNEXT
                CSPECTMAP       "kube.map"

;;----------------------------------------------------------------------------------------------------------------------
;; Includes

                include "src/z80n.s"

                org     $c000

                include "src/dma.s"
                include "src/video.s"
                include "src/render.s"
                include "src/maths.s"

;;----------------------------------------------------------------------------------------------------------------------
;; Start
                
Start:
                di                      ; Disable interrupts
                ld      sp,0            ; Set up stack
                xor     a
                out     ($fe),a         ; Set border to black
                call    initVideo       ; Initialise Layer 2 256x192 mode
                call    clearScreen     ; Clear the screen
                call    present         ; Then present that cleared screen

                ld      a,32
                call    sin0_8
                call    sign_ext
                break



                xor     a
.l1:
                out     ($fe),a
                inc     a
                and     7
                push    af
                call    clearScreen
                call    render
                call    present
                pop     af

                jr      .l1

                display "Final address (Max = $feff): ",$," Space: ",$10000-$

;;----------------------------------------------------------------------------------------------------------------------
;; End of code

                org     $ff00
Stack           ds      256


;;----------------------------------------------------------------------------------------------------------------------
;; Nex file generation

                ; Save nex information (filename, start PC, start SP)
                SAVENEX OPEN "kube.nex", Start, $0000

                ; Minimum hardware 2.0.0
                SAVENEX CORE 2,0,0

                ; 1st parameter: Border colour
                ; 2nd parameter: File handle (0=open, 1=close or address=write handle to this address)
                ; 3rd parameter: Preserve next registers (0 = set to default state)
                ; 4th parameter: 2MB RAM required? (0 = no, 1 = yes)
                SAVENEX CFG 0, 1, 0, 0

                ; 16k banks to be written
                SAVENEX AUTO

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
