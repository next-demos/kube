;;----------------------------------------------------------------------------------------------------------------------
;; 3D rendering routines
;;----------------------------------------------------------------------------------------------------------------------

;; TODO: Page align XBuf

;;----------------------------------------------------------------------------------------------------------------------
;; XBuf
;; This contains the start x and end x coordinate of each raster line that needs to be drawn to the screen
;;----------------------------------------------------------------------------------------------------------------------

XBuf            ds      256*2
MinY            db      0               ; Minimum Y coordinate written into XBuf
MaxY            db      0               ; Maximum Y coordinate written into XBuf

;;----------------------------------------------------------------------------------------------------------------------
;; Coords
;; This buffer contains the number of coordinates for the polygon (maximum 8 coordinates)
;;----------------------------------------------------------------------------------------------------------------------

NumCoords       db      4
;Coords          db      1,1,120,45,30,90,1,1
;Coords          db      1,1,30,15,10,30,1,1

;Coords          db      0,0,255,0,0,191,0,0
Coords          db      10,100,10,10,50,60,90,10,90,100

;;----------------------------------------------------------------------------------------------------------------------
;; PixelColour
;; The colour index used for rendering the polygon
;;----------------------------------------------------------------------------------------------------------------------

PixelColour     db      $c0

;;----------------------------------------------------------------------------------------------------------------------
;; render
;; Render the polygon given by the data in NumCoords, Coords & PixelColour
;;----------------------------------------------------------------------------------------------------------------------

render:
                //
                // Loop over the coords
                //

                ld      a,(NumCoords)
                ld      b,a
                ld      ix,Coords
.l1:
                push    bc
                call    drawLine
                pop     bc
                inc     ix
                inc     ix
                djnz    .l1
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; drawLine
;;
;; Original routine written by Matt Davies.
;; Optimised by:
;;      Matt Davies
;;      Peter Ped Helcmanovsky
;;      Michael Flash Ware
;;
;; X0 = IX+0
;; Y0 = IX+1
;; X1 = IX+2
;; Y1 = IX+3
;;
;; Algorithm:
;;      steep = false
;;      if (x0 > x1)
;;      {
;;        swap(x0,x1)
;;        swap(y0,y1)
;;      }
;;      dx = abs(x1 - x0)
;;      dy = abs(y1 - y0)
;;      if (dy > dx)
;;      {
;;        steep = true
;;        swap(dx,dy)
;;      }
;;
;;      signed err = 2*dy - m
;;      e = 2*dy
;;      f = 2*(dy - dx)
;;      step = (y1 < y2) ? 1 : -1;
;;      y = y0
;;
;;      for (x = x0; x <= x1; ++x)
;;      {
;;        if (steep) pixel(y,x) else pixel(x,y)
;;        if (err <= 0)
;;        {
;;          err +-= e
;;        }
;;        else
;;        {
;;          err += f
;;          y += step
;;        }
;;      }


drawLine:
                //
                // Calculate dx and dy
                //

                ; Line is (E,D) -> (L, H)

                ld      de,(ix+0)       ; DE = YX0
                ld      hl,(ix+2)       ; HL = YX1

                ld      a,(PixelColour)
                ld      (.smc_pix+1),a

                ; Swap end points if we're drawing in the wrong direction
                ld      a,e
                cp      l               ; x0 < x1?
                jr      c,.dir_ok1

                ; No swap end points
                ex      de,hl
.dir_ok1:

                ;
                ; Calculate DX
                ;
                ld      a,l
                sub     e               ; x1 - x0
                jr      nc,.pos1        ; x1 > x0?
                ; x0 > x1
                neg

.pos1:          ld      (.dx),a

                ;
                ; Calculate DY
                ;
                ld      a,h
                sub     d               ; y1 - y0
                jr      nc,.pos2        ; y1 > y0?
                ; y0 > y1
                neg

.pos2:          ld      (.dy),a

                ;
                ; Are we steep?
                ; If, so we transpose the coordinate system
                ;

                ld      c,a
                ld      a,(.dx)
                cp      c               ; dx < dy?  If so, it's steep
                ld      bc,.ns_table
                jr      nc,.not_steep

                ;
                ; Steep version
                ;

                ; Swap dx and dy
                ld      bc,(.dx)        ; C = dx, B = dy
                swap8   b,c
                ld      (.dx),bc
                ld      bc,.s_table

.not_steep:
                ; Modify the draw routine according to the octant the line is in
                ld      a,h
                cp      d               ; CF = 1 if y1 < y0
                ld      a,0
                rla
                add     a,a
                add     bc,a            ; BC = SMC table

                ld      a,(bc)
                ld      (.smc_next),a
                inc     c               ; Table is aligned to 2, so this is safe
                ld      a,(bc)
                ld      (.smc_step),a

                push    de

                ; Calculate the deltas and errors

                ; Calculate initial error value (-dx-1)
                ld      a,(.dx)
                ld      yl,a            ; IYL is the count
                cpl
                ld      h,$ff
                ld      l,a             ; HL = -dx-1
                ld      (.err),hl

                ; Calculate error adjust on step (-2dx)
                inc     hl              ; HL = -dx
                add     hl,hl
                ld      (.f),hl

                ; Calculate error adjust on non-step (2dy) and put it in BC
                ld      a,(.dy)
                ld      l,a
                ld      h,0
                add     hl,hl
                ld      bc,hl

                ;-------------------------------------------------------------------------------------------------------
                ; Draw line
                ;-------------------------------------------------------------------------------------------------------

                pop     de              ; DE = screen address
                inc     yl              ; Include the final pixel in the count
                ld      hl,(.err)       ; hl = error
.smc_pix:       ld      a,0
.l1:
                ld      (de),a

                ; Have we reached the end?
                dec     yl
                ret     z

.smc_next:      inc     e               ; increment X

                add     hl,bc
                jp      nc,.l1

                ; Adjust error and move to next line
.smc_step:      inc     d
.f              equ     $+2
                add     hl,0
                jp      .l1

                ;-------------------------------------------------------------------------------------------------------

.dx             db      0
.dy             db      0

.err            dw      0

                align   2               ; Aligned so we can use 'inc c' above
.ns_table:
                inc     e
                inc     d
                inc     e
                dec     d
.s_table:
                inc     d
                inc     e
                dec     d
                inc     e
