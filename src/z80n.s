;;----------------------------------------------------------------------------------------------------------------------
;; Z80 and Next macros and registers
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Pseudo-instructions

; Acts like: LD HL,(HL)
;
ldhl            macro
                ld      a,(hl)
                inc     hl
                ld      h,(hl)
                ld      l,a
                endm

ldde            macro
                ld      e,(hl)
                inc     hl
                ld      d,(hl)
                dec     hl
                endm

bchilo          macro   hi, lo
                ld      bc,(hi * 256) + lo
                endm

dehilo          macro   hi, lo
                ld      de,(hi * 256) + lo
                endm

hlhilo          macro   hi, lo
                ld      hl,(hi * 256) + lo
                endm

readreg         macro   reg
                ld      bc,IO_REG_SELECT
                ld      a,reg
                out     (c),a
                ld      bc,IO_REG_ACCESS
                in      a,(c)
                endm

callhl          macro
                push    $+5
                jp      (hl)
                endm

swap8           macro   r1,r2
                ld      a,r1
                ld      r1,r2
                ld      r2,a
                endm

swap16          macro   r1,r2
                push    r1
                push    r2
                pop     r1
                pop     r2
                endm

brka            macro   ch
                cp      ch
                jr      nz,$+4
                break
                endm

;;----------------------------------------------------------------------------------------------------------------------
;; esxDOS compatible API calls
;; In DOT commands, use RST $08 to call these.

DISK_FILEMAP    equ     $85
DISK_STRMSTART  equ     $86
DISK_STRMEND    equ     $87

M_DOSVERSION    equ     $88
M_GETSETDRV     equ     $89
M_TAPEIN        equ     $8b
M_TAPEOUT       equ     $8c
M_GETHANDLE     equ     $8d     ; Exit: A=handle, CF=0
M_GETDATE       equ     $8e
M_EXECCMD       equ     $8f
M_SETCAPS       equ     $91
M_DRVAPI        equ     $92
M_GETERR        equ     $93
M_P3DOS         equ     $94     ; +3 DOS function call
M_ERRH          equ     $95

F_OPEN          equ     $9a     ; Entry: A=drive, HL=filespec, B=Access mode, Exit: CF=0: A=handle, CF=1: A=error code
F_CLOSE         equ     $9b
F_SYNC          equ     $9c
F_READ          equ     $9d     ; Entry: A=handle, HL=address, BC=bytes, Exit: BC=bytes read, CF=0: HL=after bytes, CF=1: A=error code
F_WRITE         equ     $9e
F_SEEK          equ     $9f
F_GETPOS        equ     $a0
F_FSTAT         equ     $a1
F_FTRUNCATE     equ     $a2
F_OPENDIR       equ     $a3
F_READDIR       equ     $a4
F_TELLDIR       equ     $a5
F_SEEKDIR       equ     $a6
F_REWINDDIR     equ     $a7
F_GETCWD        equ     $a8
F_CHDIR         equ     $a9
F_MKDIR         equ     $aa
F_RMDIR         equ     $ab
F_STAT          equ     $ac
F_UNLINK        equ     $ad 
F_TRUNCATE      equ     $ae
F_CHMOD         equ     $af
F_RENAME        equ     $b0
F_GETFREE       equ     $b1

dos             macro   func
                rst     8
                db      func
                endm

FA_READ                 equ     $01
FA_WRITE                equ     $02
FA_RWP3HDR              equ     $40     ; Include +3 dos header.
FA_OPEN_EXISTING        equ     $00     ; Open an existing file, but error if not existing.
FA_OPEN                 equ     $08     ; Open an existing file or create a new one.
FA_CREATE               equ     $04     ; Create a new file or error if it already exists.
FA_CREATE_NEW           equ     $0c     ; Create a new file, deleting it if it already exists.


;;----------------------------------------------------------------------------------------------------------------------
;; NextZXOS APIs


IDE_BANK        equ     $01bd           ; NextZXOS function to manage memory
DOS_SET_1346    equ     $013f



;;----------------------------------------------------------------------------------------------------------------------
;; I/O Ports


; Communication and memory
IO_I2C_CLOCK    equ     $103b
IO_I2C_DATA     equ     $113b
IO_UART_TX      equ     $133b
IO_UART_RX      equ     $143b
IO_DMA          equ     $6b

; Paging
IO_PLUS3_PAGE   equ     $1ffd
IO_128K_PAGE    equ     $7ffd
IO_NEXT_PAGE    equ     $dffd

; Next registers
IO_REG_SELECT   equ     $243b
IO_REG_ACCESS   equ     $253b

; Graphics
IO_LAYER2       equ     $123b
IO_SPRITE       equ     $303b
IO_SPRITE_ATTR  equ     $57
IO_SPRITE_PATT  equ     $5b
IO_ULA          equ     $fe
IO_TIMEX        equ     $ff

; Audio
IO_AUDIO        equ     $bffd
IO_TURBO_SOUND  equ     $fffd
IO_SPECDRUM     equ     $df

; Input
IO_KEYBOARD     equ     $fe
IO_MOUSE_BTNS   equ     $fadf
IO_MOUSE_X      equ     $fbdf
IO_MOUSE_Y      equ     $ffdf
IO_JOY0         equ     $1f
IO_JOY1         equ     $37

;;----------------------------------------------------------------------------------------------------------------------
;; Next registers

NR_MACHINE_ID   equ     $00
NR_VERSION      equ     $01
NR_RESET        equ     $02
NR_MACHINE_TYPE equ     $03
NR_ROM_MAP      equ     $04
NR_PERIPH_1     equ     $05
NR_PERIPH_2     equ     $06
NR_CLOCK_SPEED  equ     $07
NR_PERIPH_3     equ     $08
NR_PERIPH_4     equ     $09
NR_VERSION_2    equ     $0e
NR_ANTI_BRICK   equ     $10
NR_VIDEO_TIME   equ     $11
NR_L2_PAGE      equ     $12
NR_L2S_PAGE     equ     $13
NR_TRANSP       equ     $14
NR_SPRITE_CTRL  equ     $15
NR_L2_X         equ     $16
NR_L2_Y         equ     $17
NR_CLIP_L2      equ     $18
NR_CLIP_SPRITES equ     $19
NR_CLIP_ULA     equ     $1a
NR_CLIP_TILEMAP equ     $1b
NR_CLIP_CTRL    equ     $1c
NR_RASTER_MSB   equ     $1e
NR_RASTER_LSB   equ     $1f
NR_RASTER_INTC  equ     $22
NR_RASTER_INTV  equ     $23
NR_KEYMAP_ADDRH equ     $2a
NR_KEYMAP_ADDRL equ     $2b
NR_KEYMAP_DATAH equ     $2c
NR_KEYMAP_DATAL equ     $2d
NR_TMAP_XMSB    equ     $2f
NR_TMAP_XLSB    equ     $30
NR_TMAP_Y       equ     $31
NR_ULA_X        equ     $32
NR_ULA_Y        equ     $33
NR_SPRITE_IDX   equ     $34
NR_SPRITE_0     equ     $35
NR_SPRITE_1     equ     $36
NR_SPRITE_2     equ     $37
NR_SPRITE_3     equ     $38
NR_SPRITE_4     equ     $39
NR_PAL_IDX      equ     $40
NR_PAL_VAL      equ     $41
NR_EULA_INKMASK equ     $42
NR_EULA_CTRL    equ     $43
NR_EULA_PAL     equ     $44
NR_TRANSPARENT  equ     $4a
NR_SPRITE_TRANS equ     $4b
NR_TMAP_TRANS   equ     $4c
NR_MMU0         equ     $50
NR_MMU1         equ     $51
NR_MMU2         equ     $52
NR_MMU3         equ     $53
NR_MMU4         equ     $54
NR_MMU5         equ     $55
NR_MMU6         equ     $56
NR_MMU7         equ     $57
NR_COPPER       equ     $60
NR_COPPER_CTRLL equ     $61
NR_COPPER_CTRLH equ     $62
NR_ULA_CTRL     equ     $68
NR_DISP_CTRL    equ     $69 
NR_LORES_CTRL   equ     $6a
NR_TMAP_CTRL    equ     $6b
NR_TMAP_DEFATTR equ     $6c
NR_TMAP_BASE    equ     $6e
NR_TILES_BASE   equ     $6f
NR_L2_CTRL      equ     $70
NR_SPRITE_0_INC equ     $75
NR_SPRITE_1_INC equ     $76
NR_SPRITE_2_INC equ     $77
NR_SPRITE_3_INC equ     $78
NR_SPRITE_4_INC equ     $79
NR_LED          equ     $ff

; Clock speed
CLOCK_3_5MHZ    equ     0
CLOCK_7MHZ      equ     1
CLOCK_14MHZ     equ     2
CLOCK_28MHZ     equ     3

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
