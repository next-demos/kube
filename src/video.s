;;----------------------------------------------------------------------------------------------------------------------
;; Video control
;;
;; The video system reserves these pages:
;;
;;      16-21   L2 VRAM
;;      22-27   Shadow L2 VRAM
;;
;; TODO:
;;      - Use copper to set L2 VRAM banks
;;
;;----------------------------------------------------------------------------------------------------------------------

DisplayBank     db      8
DrawBank        db      11

;;----------------------------------------------------------------------------------------------------------------------
;; initVideo
;; Set up 256x192 Layer 2 from $0000-$bfff

initVideo:
                ; Initialise the L2 palette
                nextreg NR_EULA_CTRL,%0'001'0000        ; Layer 2 palette 0
                xor     a
                nextreg NR_PAL_IDX,a                    ; Set to output 1st colour in palette (index 0)
.l1             nextreg NR_PAL_VAL,a
                inc     a
                jr      nz,.l1

                ; Set L2 VRAM at pages 16-21, and at pages 22-27
                nextreg NR_L2_PAGE,8
                nextreg NR_L2S_PAGE,11

                ; Initialise L2
                ;
                ; IO Port 123b:
                ;       7-6     VRAM bank select (write/read paging) or 3 for mapping whole L2 to MMU0-5
                ;       5-4     0
                ;       3       Use shadow L2 for paging
                ;       2       Enable read-only paging (as bit 0 but with RO mode)
                ;       1       L2 visible
                ;       0       Enable L2 write-only paging  (section of L2 in bits 7-6 mapped to MMU0-1 in WO mode)
                ;
                ld      bc,IO_LAYER2
                ld      a,%11'00'1111           ; Map L2 memory to $0000-$bfff and make it visible
                out     (c),a

                call    clearScreen
                call    present
                call    clearScreen
                call    present

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; clearScreen

clearScreen:
                xor     a
                ld      hl,0
                ld      bc,$c000
                call    memfill
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; present
;; Present the shadow screen
;;

present:
                ;readreg NR_RASTER_MSB
                ;and     1
                ;jr      z,present
                ;readreg NR_RASTER_LSB
                ;and     a
                ;jr      nz,present

                ; Switch the display and draw banks
                ld      a,(DisplayBank)
                nextreg NR_L2S_PAGE,a           ; The old display banks are the new draw banks
                ld      b,a
                ld      a,(DrawBank)
                nextreg NR_L2_PAGE,a            ; The old draw banks are now the new display banks
                ld      (DisplayBank),a
                ld      a,b
                ld      (DrawBank),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
