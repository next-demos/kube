;;----------------------------------------------------------------------------------------------------------------------
;; Maths routines
;;----------------------------------------------------------------------------------------------------------------------

                align   256

sin_table:
    lua allpass
        for i = 0, 255+128, 1 do
            sj.add_byte(math.floor(math.sin(math.pi * i / 128.0) * 127.5))
        end
    endlua

;;----------------------------------------------------------------------------------------------------------------------
;; sin
;; Calculate SIN using bytians (0-255 for full circle)
;;
;; Input:
;;      A = angle
;;
;; Output:
;;      A = signed 0.8 fixed-point value
;;

sin0_8:
                ld      h,high sin_table
                ld      l,a
                ldhl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; cos
;; Calculate COS using bytians (0-255 for full circle)
;;
;; Input:
;;      A = angle
;;
;; Output:
;;      A = signed 0.8 fixed-point value
;;

cos0_8:
                ld      hl,high sin_table
                add     a,128
                ld      l,a
                ldhl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; sign_ext
;; Sign-extend an signed 0.8 value to a signed 16.8 value
;;
;; Input:
;;      A = value
;;
;; Output:
;;      DEH = 16.8 value
;;

sign_ext:
                add     a,a
                ld      h,a
                sbc     a,a
                ld      d,a
                ld      e,a
                ret
