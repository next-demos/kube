@echo off
call m.bat
if not errorlevel 1 (
    rem Run nex file directly in CSpect
    \env\cspect\CSpect.exe -basickeys -r -tv -brk -exit -16bit -s28 -w3 -zxnext -map=kube.map -mmc=\env\tbblue.mmc kube.nex
)
