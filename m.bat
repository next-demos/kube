@echo off
\env\sjasmplus\sjasmplus src\main.s -I/env/src --zxnext=cspect --msg=war --fullpath
if not errorlevel 1 (
    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc kube.nex /
    call i.bat
)
